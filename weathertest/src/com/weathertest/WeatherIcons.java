package com.weathertest;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class WeatherIcons {
	public enum Condition { THUNDERSTORM, RAIN, SNOW, WIND, OVERCAST, 
		PARTLY_SUN, PARTLY_MOON, SUN, MOON, UNKNOWN }; 
		
	private static final Map<Condition, Integer> iconMap;
    static {
        Map<Condition, Integer> aMap = new HashMap<Condition, Integer>();
        aMap.put(Condition.THUNDERSTORM, R.drawable.i00);
        aMap.put(Condition.RAIN, R.drawable.i01);
        aMap.put(Condition.SNOW, R.drawable.i14);
        aMap.put(Condition.WIND, R.drawable.i23);
        aMap.put(Condition.OVERCAST, R.drawable.i26);
        aMap.put(Condition.PARTLY_MOON, R.drawable.i29);
        aMap.put(Condition.PARTLY_SUN, R.drawable.i30);
        aMap.put(Condition.MOON, R.drawable.i31);
        aMap.put(Condition.SUN, R.drawable.i32);
        aMap.put(Condition.UNKNOWN, R.drawable.i25);
        iconMap = Collections.unmodifiableMap(aMap);
    }

    
    private static final Map<String, Condition> conditionMap;
    static {
    	boolean isDay = isDay();
        Map<String, Condition> aMap = new HashMap<String, Condition>();
        aMap.put("Clear", isDay ? Condition.SUN : Condition.MOON);
        aMap.put("Partly Cloudy", isDay ? Condition.PARTLY_SUN : Condition.PARTLY_MOON);
        aMap.put("Cloudy", Condition.OVERCAST);
        aMap.put("Overcast", Condition.OVERCAST);
        aMap.put("Hazy", isDay ? Condition.SUN : Condition.MOON);
        aMap.put("Foggy", Condition.OVERCAST);
        aMap.put("Rain", Condition.RAIN);
        aMap.put("Snow", Condition.SNOW);
        conditionMap = Collections.unmodifiableMap(aMap);
    }

	public static int getConditionResource(String strCond) {
		Condition cond = conditionMap.get(strCond);
		if (cond != null) {
			Integer resource = iconMap.get(cond);
			if (resource != null) {
				return resource;
			}
		}
		
		return getErrorResource();
	}
	
	/**
	 * Is it day or night time? This is to be used for deciding which icons to use
	 * @return true if it's day time
	 */
	public static boolean isDay() {
		//TODO: implement something that looks at the current time and returns true if it's after 6am and before 8pm
		Calendar rightNow = Calendar.getInstance();
		int hour = rightNow.get(Calendar.HOUR_OF_DAY);
		if (hour > 6 && hour < 20) return true; else return false;
	}

	public static int getErrorResource() {
		return iconMap.get(Condition.UNKNOWN);
	}
}
