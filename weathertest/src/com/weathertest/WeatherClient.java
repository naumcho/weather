package com.weathertest;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.weathertest.WeatherCond;

public class WeatherClient {

//	private final String URI = "http://rss.weather.com/weather/rss/local/11211?cm_ven=LWO&cm_cat=rss&par=LWO_rss";
	private final String URI = "http://api.wunderground.com/api/9b185b12f417a565/conditions/q/60068.xml";
	
	private final DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
	
	XPath xpath = XPathFactory.newInstance().newXPath();
//	String expression = "/rss/channel/item[1]/description";
	private final String tempStr = "/response/current_observation/temp_f";
	private final String condStr = "/response/current_observation/weather";
	private final String locStr = "/response/current_observation/display_location/full";

	public WeatherClient()  {
		builderFactory.setNamespaceAware(true);

	}
	

	
	public WeatherCond getWeather() throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {
		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		Document document = builder.parse(URI);
		
		Node tempNode = (Node) xpath.evaluate(tempStr, document, XPathConstants.NODE);
		String temp = tempNode.getTextContent();
		
		Node condNode = (Node) xpath.evaluate(condStr, document, XPathConstants.NODE);
		String cond = condNode.getTextContent();
		
		Node locNode = (Node) xpath.evaluate(locStr, document, XPathConstants.NODE);
		String loc = locNode.getTextContent();
		
		WeatherCond currentWeather = new WeatherCond(temp, cond, loc);
		System.out.println(currentWeather.toString());
		return currentWeather;
	}
	
}
