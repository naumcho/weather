package com.weathertest;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

import com.weathertest.WeatherCond;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.NetworkOnMainThreadException;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;


public class MainActivity extends Activity {
	
	private final WeatherClient wc = new WeatherClient();
	private ImageButton refreshButton;
	private TextView currentTempText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
    	refreshButton = (ImageButton) findViewById(R.id.refreshButton);
    	currentTempText = (TextView) findViewById(R.id.currentTemperature);
    	
    	getWindow().getDecorView().setBackgroundColor(Color.BLACK);

    	//TODO: save what the condition was last time and display that at first while loading
    	refreshButton.setImageResource(WeatherIcons.getErrorResource());
        refreshButton.setOnClickListener(new View.OnClickListener() {
			
			@SuppressLint("NewApi")
			@Override
			public void onClick(View arg0)  {

				new AsyncTask<Integer, Void, WeatherCond>(){
			        @Override
			        protected WeatherCond doInBackground(Integer... params) {
			        	
						try {
							WeatherCond currentW = wc.getWeather();
							return currentW;

						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (ParserConfigurationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SAXException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (NetworkOnMainThreadException e) {
							e.printStackTrace();
						}			            
			            return null;
			        }

					@Override
					protected void onPostExecute(WeatherCond result) {
						// TODO Auto-generated method stub
						super.onPostExecute(result);
						
						if (result != null) {
							currentTempText.setText(result.toString());						
							refreshButton.setImageResource(WeatherIcons.getConditionResource(result.cond));
						} else {
							currentTempText.setText("Error");
							refreshButton.setImageResource(WeatherIcons.getErrorResource());
						}
					}
			        
			    }.execute(1);
					

					

				
			}
		});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
