package com.weathertest;

public class WeatherCond {
	@Override
	public String toString() {
		return "WeatherCond [temp=" + temp + ", cond=" + cond + ", loc="
				+ loc + "]";
	}

	public String temp; //temperature
	public String cond; //condition - e.g. sunny, overcast, etc
	public String loc; //location
	
	public WeatherCond(String temp, String cond, String loc) {
		this.temp = temp;
		this.cond = cond;
		this.loc = loc;
	}
}